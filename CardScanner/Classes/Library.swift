//
//  Library.swift
//  CardScanner
//
//  Created by Артём Шляхтин on 31/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

public struct FrameworkCardReader {
    public static let bundleName = "ru.roseurobank.CardScanner"
}

@objc public protocol CardScannerDelegate: NSObjectProtocol {
    func cardScanner(_ controller: CardScannerController, didScanCard cardInfo: Dictionary<String, AnyObject>)
    
    /** Задает настройки цвета рамки. */
    @objc optional func cardScannerCadreColor() -> UIColor
}
