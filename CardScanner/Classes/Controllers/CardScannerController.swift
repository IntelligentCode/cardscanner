//
//  ReaderController.swift
//  CardReader
//
//  Created by Артем Шляхтин on 05/10/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
import System
import CardIO

public class CardScannerController: UIViewController {
    
    @IBOutlet weak var scanner: ScannerView!
    public weak var delegate: CardScannerDelegate?
    public var tag: Int = 0
    
    
    // MARK: - Life Cycle
    
    public override func loadView() {
        super.loadView()
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareScanner()
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configScannerBackgroundColor()
        configCadreColor()
    }
    
    public override var prefersStatusBarHidden : Bool {
        return false
    }
    
    public override var shouldAutorotate : Bool {
        return false
    }
    
    @IBAction func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private
    
    fileprivate func prepareScanner() {
        CardIOUtilities.preload()
        if CardIOUtilities.canReadCardWithCamera() {
            self.scanner.delegate = self
        }
    }
    
    fileprivate func configScannerBackgroundColor() {
        let navBar = UINavigationBar.appearance()
        if let brandColor = navBar.barTintColor,
           let currentNavBar = self.navigationController?.navigationBar
        {
            let emptyImage = UIImage()
            currentNavBar.setBackgroundImage(emptyImage, for: UIBarMetrics.default)
            currentNavBar.shadowImage = emptyImage
            
            self.scanner.backgroundColor = brandColor
        }
    }
    
    fileprivate func configCadreColor() {
        if let color = delegate?.cardScannerCadreColor?() {
            self.scanner.guideColor = color
        } else {
            let navBar = UINavigationBar.appearance()
            guard let brandColor = navBar.barTintColor else { return }
            self.scanner.guideColor = brandColor
        }
    }
    
}

// MARK: - Card Reader Delegate

extension CardScannerController: CardIOViewDelegate {
    
    public func cardIOView(_ cardIOView: CardIOView!, didScanCard cardInfo: CardIOCreditCardInfo!) {
        guard let info = cardInfo else { return }
        guard let number = info.cardNumber.formatCreditCardNumber else { return }
        
        var expiry = ""
        if info.expiryMonth > 0 && info.expiryYear > 0 {
            expiry = String(format: "%02lu / %02lu", info.expiryMonth, info.expiryYear % 100)
        }
        
        let dict:[String: AnyObject] = ["Card Number":number as AnyObject, "Card Expiry":expiry as AnyObject, "Expiry Month": info.expiryMonth as AnyObject, "Expiry Year": info.expiryYear as AnyObject]
        self.delegate?.cardScanner(self, didScanCard: dict)
        self.dismiss(animated: true, completion: nil)
    }
    
}
