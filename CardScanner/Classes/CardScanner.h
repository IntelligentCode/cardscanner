//
//  CardScanner.h
//  CardScanner
//
//  Created by Артём Шляхтин on 31/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CardScanner.
FOUNDATION_EXPORT double CardScannerVersionNumber;

//! Project version string for CardScanner.
FOUNDATION_EXPORT const unsigned char CardScannerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CardScanner/PublicHeader.h>

@import AudioToolbox;
@import AVFoundation;
@import CoreMedia;
@import CoreVideo;
@import MobileCoreServices;
@import Accelerate;
