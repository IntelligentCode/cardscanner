//
//  ScannerView.swift
//  CardReader
//
//  Created by Артем Шляхтин on 31.08.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
import CardIO

class ScannerView: CardIOView {
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.performInitializations()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.performInitializations()
    }
    
    
    // MARK: - Public
    
    func refresh() {
        self.isHidden = true
        self.isHidden = false
    }
    
    // MARK: - Private
    
    fileprivate func performInitializations() {
        self.configure()
    }
    
    fileprivate func configure() {
        self.hideCardIOLogo = true
    }
    
}
